# camagru

# BUILD : docker build -t triviere/camagru .
# LAUNCH : docker run --rm -it -p 8000:80 -p 3306:3306 -v $(pwd)/www:/var/www/html triviere/camagru bash

FROM debian

MAINTAINER triviere <triviere@student.42.fr>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get -y install php5
RUN apt-get -y install mysql-server
RUN apt-get -y install mysql-client
RUN apt-get -y install apache2
RUN apt-get -y install php5-mysql

RUN sed -i "s/127.0.0.1/0.0.0.0/g" /etc/mysql/my.cnf
RUN sed -i "s/Listen 80/Listen 0.0.0.0:80/g" /etc/apache2/ports.conf
RUN sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf
RUN echo "ServerName camagru" >> /etc/apache2/apache2.conf
RUN a2enmod rewrite

RUN sed -i "s/display_startup_errors = Off/display_startup_errors = On/g" /etc/php5/apache2/php.ini
RUN sed -i "s/display_errors = Off/display_errors = On/g" /etc/php5/apache2/php.ini

ADD setup.sh /usr/local/bin/setup.sh
RUN chmod +x /usr/local/bin/setup.sh

EXPOSE 80
EXPOSE 3306

WORKDIR /var/www/html

ENTRYPOINT ["setup.sh"]
CMD ["bash"]
