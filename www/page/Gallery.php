<?php

namespace Page;


class Gallery extends Master {

	public function gallery() {}
	public function me() {}
	public function picture() {}
	public function newWebcamPicture() {}
	public function newUploadPicture() {}

	public function comment() {
		$image = $this->getSingleImage();
		if ($image !== NULL) {
			if (isset($_POST['comment']) && !empty($_POST['comment'])) {
				$this->_db->exec('INSERT INTO comment (id_user, id_montage, value) VALUES (:me, :picture, :comment)', [
					'me' => $this->_user->getId(),
					'picture' => $this->getParam(),
					'comment' => strip_tags($_POST['comment'])
				]);
				if ($image['id'] != $this->_user->getId()) {
					mail($image['email'], 'Camagru - Nouveau commentaire', $this->_user->getLogin(). ' à commenté votre image : http://localhost:8080/gallery/'. $image['img_id']);
				}
			}
		}
		header('Location: /gallery/'. $this->getParam());
		die();
	}

	public function delete() {
		$image = $this->getSingleImage();
		if ($image) {
			if ($image['id'] == $this->_user->getId()) {
				$this->_db->exec('DELETE FROM montage WHERE id=:picture', ['picture' => $image['img_id']]);
			}
		}
		header('Location: /gallery/me');
		die();
	}

	public function getPagingation() {
		return $this->getAllMontages($this->getParam());
	}

	public function getMyPagingation() {
		return $this->getMyMontages($this->getParam());
	}

	public function getComments() {
		return $this->_db->get('SELECT c.value, u.login, u.email, u.id FROM comment AS c
			JOIN user AS u
				ON u.id=c.id_user
			WHERE c.id_montage=:picture
		ORDER BY id DESC;', [
			'picture' => $this->getParam()
		]);
	}

	public function getAllMontages($page = 1) {
		if ($page < 1)
			return [];
		$query = "SELECT m.id as img_id, u.login, u.email, MD5(u.email) AS avatar, u.id, (SELECT COUNT(*) FROM thumb WHERE id_montage=m.id AND id_user=:my_id) AS liked FROM montage AS m
			JOIN user AS u
				ON u.id=m.id_user
		ORDER BY m.id DESC
		LIMIT ". (($page - 1) * 5) .", 5;";
		return $this->_db->get($query, [
			'my_id' => $this->_user->getId()
		]);
	}

	public function getMyMontages($page = 1) {
		if ($page < 1)
			return [];
		$query = 'SELECT m.id as img_id, u.login, u.email, MD5(u.email) AS avatar, u.id, (SELECT COUNT(*) FROM thumb WHERE id_montage=m.id AND id_user=:my_id) AS liked FROM montage AS m
			JOIN user AS u
				ON u.id=m.id_user
			WHERE m.id_user=:my_id
		ORDER BY m.id DESC
		LIMIT '. (($page - 1) * 5) .', 5;';
		return $this->_db->get($query, ['my_id' => $this->_user->getId() ]);
	}

	public function getSingleImage() {
		$query = 'SELECT m.id as img_id, u.*, (SELECT COUNT(*) FROM thumb WHERE id_montage=m.id AND id_user=:my_id) AS liked FROM montage AS m
			JOIN user AS u
				ON u.id=m.id_user
			WHERE m.id=:current;';
		$currents = $this->_db->get($query, ['current' => $this->getParam(), 'my_id' => $this->_user->getId() ]);
		if (!count($currents))
			return NULL;
		return $currents[0];
	}

	public function like() {
		$result = ['success' => false, 'message' => 'Vous aimé deja ce montage'];
		if ($this->getSingleImage() === NULL)
			$result['message'] = "Cette image n'existe pas";
		else {
			$picture = $this->getParam();
			$liked = $this->_db->get('SELECT * FROM thumb WHERE id_montage=:picture AND id_user=:me', ['picture' => $picture, 'me' => $this->_user->getId()]);
			if (!count($liked)) {
				$this->_db->exec('INSERT INTO thumb (id_montage, id_user) VALUES (:picture, :me)', ['picture' => $picture, 'me' => $this->_user->getId()]);
				$result['success'] = true;
				$result['message'] = "Vous aimer maintenant ce montage";
			}
		}
		return $result;
	}

	public function unlike() {
		$result = ['success' => false, 'message' => 'Vous aimé deja pas ce montage'];
		if ($this->getSingleImage() === NULL)
			$result['message'] = "Cette image n'existe pas";
		else {
			$picture = $this->getParam();
			$liked = $this->_db->get('SELECT * FROM thumb WHERE id_montage=:picture AND id_user=:me', ['picture' => $picture, 'me' => $this->_user->getId()]);
			if (count($liked)) {
				$this->_db->exec('DELETE FROM thumb WHERE id_montage=:picture AND id_user=:me', ['picture' => $picture, 'me' => $this->_user->getId()]);
				$result['success'] = true;
				$result['message'] = "Vous n'aimer maintenant plus ce montage";
			}
		}
		return $result;
	}

	public function getElements() {
		$handle = opendir(__DIR__ .'/../public/img/component/');
		$pictures = [];
		if (!$handle)
			return $pictures;
		while (($file = readdir($handle)) !== false) {
			if ($file == '.' || $file == '..' || $file == '.DS_Store')
				continue ;
			$pictures[] = [ 'url' => $file ];
		}
		return $pictures;
	}


	public function newWebcamPictureAction() {
		$result = ['success' => false, 'message' => ''];

		if (!isset($_POST['img']) || !isset($_POST['montages']) || empty($_POST['montages']) || empty($_POST['img']) || !is_array($_POST['montages']))
			$result['message'] = 'Il nous manque des information pour construire votre image';
		else {
			$img64 = split(',', $_POST['img']);
			if (count($img64) != 2)
				$result['message'] = "Votre format d'image n'est pas valide";
			else {
				$imageData = base64_decode($img64[1]);
				$source = $this->createImage(imagecreatefromstring($imageData), $_POST['montages'], 245, 215);
				if ($source === false)
					$result['message'] = "Une erreur s'est produite, surement un element de montage qui n'existe pas";
				else {
					$imagePath = "/img/montage/montage-". $this->_user->createImage() .".jpg";
					imagejpeg($source, __DIR__. "/../public". $imagePath);
					$result['success'] = true;
					$result['message'] = 'Votre image a bien été créer';
					$result['src'] = $imagePath;
				}
			}
		}
		return $result;
	}

	public function newUploadPictureAction() {
		if (!(!isset($_FILES['image']) || empty($_FILES['image']) || !isset($_POST['montage']) || empty($_POST['montage']) || !is_array($_POST['montage']))) {
			list($width, $height, $type, $attr) = getimagesize($_FILES['image']['tmp_name']);
			$type = image_type_to_mime_type($type);
			if ($type == 'image/png')
				$source = $this->createImage(imageCreateFromPng($_FILES['image']['tmp_name']), $_POST['montage'], $width / 2, $height / 2);
			else if ($type == 'image/jpeg')
				$source = $this->createImage(imageCreateFromJpeg($_FILES['image']['tmp_name']), $_POST['montage'], $width / 2, $height / 2);
			else
				$source = false;
			if ($source !== false) {
				$imageId = $this->_user->createImage();
				$imagePath = "/img/montage/montage-". $imageId .".jpg";
				imagejpeg($source, __DIR__. "/../public". $imagePath);
				header('Location: /gallery/'. $imageId);
				die();		
			}
		}
		header('Location: /gallery/new/upload');
		die();
	}

	private function createImage($source, $imgs, $x = 0, $y = 0) {
		foreach ($imgs as $img) {
			if (!file_exists(__DIR__ . "/../public/img/component/". $img)) {
				return false;
			}
			$imgPng = imageCreateFromPng(__DIR__ . "/../public/img/component/". $img);
			imageAlphaBlending($imgPng, true);
			imageSaveAlpha($imgPng, true);
			imagecopy($source, $imgPng, round($x), round($y), 0, 0, imagesx($imgPng), imagesy($imgPng));
		}
		return $source;
	}
}