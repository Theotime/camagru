<?php

namespace Page;

class User extends Master {
	
	private $_salt = 'goifnj#^G3yuEH(PH';
	public $validationAccount = false;

	public function signin() {}
	public function signup() {}
	public function resetPassword() {}
	public function me() {}

	public function signout() {
		setcookie('camagru', '', -1);
		header('Location: /');
	}

	public function signinAction() {
		$result = ["success" => false, "message" => ""];

		if (!isset($_POST['signin']) || empty($_POST['signin'])) {
			$result['message'] = "Veuillez remplir tous les champs";
		} else if (!isset($_POST['signin']['password']) || empty($_POST['signin']['password'])) {
			$result['message'] = 'Veuillez saisir votre password';
		} else if (!isset($_POST['signin']['login']) || empty($_POST['signin']['login'])) {
			$result['message'] = 'Veuillez saisir votre login / email';
		}  else {
			$users = $this->_db->get("SELECT * FROM user WHERE login=:login OR email=:email;", [
				'login' => strtolower($_POST['signin']['login']),
				'email' => strtolower($_POST['signin']['login'])
			]);
			if (count($users) < 1) {
				$result['message'] = 'Aucun compte ne correspond au informations que vous avez saisies';
			} else if ($users[0]['password'] != md5($_POST['signin']['password']. $this->_salt)) {
				$result['message'] = 'Il semblerais que vous ayez saisie un mauvais mots de passe';
			} else if (!$users[0]['is_actif']) {
				$result['message'] = "Cet email n'est pas actif merci de cliqué sur le liens de confirmation qui vous a été envoyer";
			} else {
				$this->_user->hydrate($users[0]);
				setcookie('camagru', $this->_user->createTokenConnexion(), time() + 3600 * 24 * 7);
				$result['success'] = true;
				$result['message'] = "Connexion success";
			}
		}
		return $result;
	}

	public function signupAction() {
		$emailReg = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		$result = ["success" => false, "message" => ""];

		if (!isset($_POST['signup']) || empty($_POST['signup'])) {
			$result['message'] = "Veuillez remplir tous les champs";
		} else if (!isset($_POST['signup']['login']) || empty($_POST['signup']['login'])) {
			$result['message'] = "Veuillez saisir votre login";
		} else if (!preg_match('/^[a-zA-Z0-9_\.-]+$/', $_POST['signup']['login'])) {
			$result['message'] = "Le format de votre login ne correspond pas au pattern suivant : [a-zA-Z0-9_\.-]+";
		} else if (!isset($_POST['signup']['email']) || empty($_POST['signup']['email'])) {
			$result['message'] = "Veuillez saisir votre email";
		} else if (!isset($_POST['signup']['password']) || empty($_POST['signup']['password'])) {
			$result['message'] = "Veuillez saisir votre password";
		} else if (!preg_match($emailReg, $_POST['signup']['email'])) {
			$result['message'] = "Votre email ne corresponds pas a un format d'email valide";
		} else if (!preg_match('/[0-9]+/', $_POST['signup']['password']) || !preg_match('/[a-zA-Z]+/', $_POST['signup']['password'])) {
			$result['message'] = "Votre mot de passe doit contenir au moin une lettre et au moin un chiffre";
		} else if (strlen($_POST['signup']['password']) < 6) {
			$result['message'] = "Votre mot de passe dois faire au minimum 6 caractères";
		} else if (strlen($_POST['signup']['login']) > 50) {
			$result['message'] = 'Votre login ne dois pas faire plus de 50 caractères';
		} else if (strlen($_POST['signup']['email']) > 150) {
			$result['message'] = 'Votre email ne dois pas faire plus de 150 caractères';
		} else {
			$users = $this->_db->get("SELECT id FROM user WHERE login=:login OR email=:email;", [
				'login' => $_POST['signup']['login'],
				'email' => $_POST['signup']['email']
			]);
			if (count($users) > 0) {
				$result['message'] = "Le login ou l'email sont déjà présent en base de données";
			} else {
				$this->_db->exec('INSERT INTO user (login, email, password) VALUES (:login, :email, :password);', [
					':login'	=> strtolower($_POST['signup']['login']),
					':email'	=> strtolower($_POST['signup']['email']),
					':password'	=> md5($_POST['signup']['password']. $this->_salt)
				]);
				$users = $this->_db->get("SELECT * FROM user WHERE login=:login OR email=:email;", [
					'login' => $_POST['signup']['login'],
					'email' => $_POST['signup']['email']
				]);
				$this->_user->hydrate($users[0]);
				$validationToken = $this->_user->createTokenValidation();
				mail($_POST['signup']['email'], 'Inscription Camagru', 'Pour confirmer votre compte merci de vous rendre a l\'adresse suivante : http://localhost:8080/validation/'. $validationToken);

				$result['success'] = true;
				$result['message'] = "Inscription réussie";
			}

		}
		return $result;
	}

	public function resetPasswordAction() {
		$result = ['success' => false, 'message' => ''];
		if (!isset($_POST['resetPassword']) || empty($_POST['resetPassword']) || !isset($_POST['resetPassword']) || empty($_POST['resetPassword']['email']))
			$result['message'] = 'Veuillez remplir tous les champs';
		else {
			$user = $this->_db->get('SELECT * FROM user WHERE email=:email;', ['email' => $_POST['resetPassword']['email']]);
			if (count($user)) {
				$this->_user->hydrate($user[0]);
				$password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 8);
				mail($this->_user->getEmail(), 'Reset password - Camagru', 'Votre nouveau mot de passe : '. $password);
				$this->_db->exec('UPDATE user SET password=:password WHERE id=:id_user;', [
					'password' 	=> md5($password. $this->_salt),
					'id_user'	=> $this->_user->getId()
				]);
				$result['message'] = 'Votre mot de passe vous a été envoyer par mail';
				$result['success'] = true;
			} else {
				$result['message'] = 'Aucun compte ne correspond a cette email';
			}
		}

		return $result;
	}

	public function mePost() {
		$result = ['success' => false, 'message' => ''];
		if (!isset($_POST['me']) || empty($_POST['me']))
			$result['message'] = 'Veuillez remplir tous les champs';
		else if (!isset($_POST['me']['oldPassword']) || empty($_POST['me']['oldPassword']))
			$result['message'] = "Il faut saisir votre ancien password afin d'en definir un nouveau";
		else if (!isset($_POST['me']['newPassword']) || empty($_POST['me']['newPassword']))
			$result['message'] = "Il faut saisir un nouveau password aussi pour le changer";
		else if (md5($_POST['me']['oldPassword']. $this->_salt) != $this->_user->getPassword()) {
			$result['message'] = "Le mot de passe saisie ne corresponds pas a celui de l'utilisateur";
		}
		else if (!preg_match('/[0-9]+/', $_POST['me']['newPassword']) || !preg_match('/[a-zA-Z]+/', $_POST['me']['newPassword']))
			$result['message'] = "Votre mot de passe doit contenir au moin une lettre et au moin un chiffre";
		else {
			$this->_db->exec('UPDATE user SET password=:password WHERE id=:id;', [
				'id'		=> $this->_user->getId(),
				'password'	=> md5($_POST['me']['newPassword']. $this->_salt)
			]);
			$result['message'] = 'Votre mot de passe a bien été changer';
			$result['success'] = true;
		}

		return $result;
	}

	public function validationAccount() {
		$this->validationAccount = false;
		$user = $this->_db->get('SELECT u.*, t.id AS id_token FROM token AS t
			LEFT JOIN user AS u
				ON u.id=t.id_user
			WHERE t.action=3 AND t.value=:token AND consume=0;', ['token' => $this->getParam()]);
		if (!count($user))
			return ;
		$this->_db->exec('UPDATE token SET consume=1 WHERE id=:id_token', ['id_token' => $user[0]['id_token']]);
		$this->_db->exec('UPDATE user SET is_actif=1 WHERE id=:id_user', ['id_user' => $user[0]['id']]);
		$this->validationAccount = true;
	}

}