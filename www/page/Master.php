<?php

namespace Page;

class Master {

	protected $_router;
	protected $_db;
	protected $_user;

	protected $_content;
	protected $_template;
	protected $_methodActive;

	protected $_param;

	public function __construct($user, $router, $db) {
		$this->_user = $user;
		$this->_db = $db;
		$this->_router = $router;
		$this->_template = __DIR__ .'/../template/'. strtolower(str_replace(['\\'], ['/'], get_class($this))) .'/'. $router->getCurrent()['methode'] .'.phtml';
		$this->_param = $router->getParam();
	}

	public function getContent() {
		include $this->_template;
	}

	public function getParam() {
		return $this->_param;
	}

}