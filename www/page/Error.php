<?php

namespace Page;

class Error extends Master {

	public function error404() {
		header("HTTP/1.0 404 Not Found");
	}

	public function error403() {
		header("HTTP/1.0 403 Not Found");
	}
	
}