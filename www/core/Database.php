<?php
namespace Core;

class Database {

	private $_dsn;
	private $_user;
	private $_password;

	private $_pdo;

	public function __construct() {
		include __DIR__ ."/../config/database.php";
		$this->_dsn = $DB_DSN;
		$this->_user = $DB_USER;
		$this->_password = $DB_PASSWORD;

		$this->_pdo = new \PDO($this->_dsn, $this->_user, $this->_password);
		$this->_pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}

	public function get($sql, $params = array()) {
		$pdo = $this->_pdo;

		$query = $pdo->prepare($sql);
		if (!empty($params))
			$query->execute($params);
		else
			$query->execute();
		return $query->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function exec($sql, $params = array()) {
		$pdo = $this->_pdo;

		$query = $pdo->prepare($sql);
		if (!empty($params))
			return $query->execute($params);
		else
			return $query->execute();
	}

	public function getPdo() {
		return $this->_pdo;
	}

}