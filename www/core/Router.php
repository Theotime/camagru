<?php

namespace Core;

class Router {

	private $_routes = NULL;
	private $_current = NULL;
	private $_user = NULL;
	private $_paramsValue = NULL;

	public function __construct($routesFile, $user) {
		$this->_routes = include $routesFile;
		$this->_user = $user;
	}

	public function match() {
		$pattern = '';
		$result = [
			"pattern"		=> "/error404",
			"class"			=> "Error",
			"methode"		=> "error404",
			"visibility"	=> "all",
			"type"			=> "page"
		];
		foreach ($this->_routes as $route) {
			$pattern = str_replace(['/'], ['\/'], $route['pattern']);
			if (preg_match('/^'. $pattern. '$/', $_SERVER['REDIRECT_URL'])) {
				if ($route['visibility'] == 'user' && !$this->_user->connected()) {
					$result['pattern'] = '/error403';
					$result['methode'] = 'error403';
					break ;
				} else if ($route['visibility'] == 'visitor' && $this->_user->connected()) {
					header('Location: /me');
					die();
				}
				preg_match_all('/^'. $pattern. '$/', $_SERVER['REDIRECT_URL'], $matches);
				if (count($matches) > 1) {
					$this->_paramsValue = $matches[1][0];
				}
				$result = $route;
				break ;
			}
		}
		$this->_current = $result;
		return $result;
	}

	public function getParam() {
		return $this->_paramsValue;
	}

	public function getCurrent() {
		return $this->_current;
	}

}