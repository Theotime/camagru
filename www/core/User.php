<?php

namespace Core;

class User {

    private $_id = NULL;
	private $_login = NULL;
    private $_email = NULL;
    private $_password = NULL;

    private $_isConnected = false;

    private $_db;

    public function __construct($db) {
        $this->_db = $db;
        if (isset($_COOKIE['camagru']) && !empty($_COOKIE['camagru']))
            $this->connectByToken($_COOKIE['camagru']);
    }

    public function connectByToken($token) {
        $users = $this->_db->get('SELECT u.* FROM token as t
            LEFT JOIN user AS u
                ON t.id_user=u.id
            WHERE t.value=:token', ['token' => $token]);
        if (count($users)) {
            $this->hydrate($users[0]);
            $this->_isConnected = true;
        }
    }

    public function hydrate($datas) {
        $this->_id = $datas['id'];
        $this->_login = $datas['login'];
        $this->_email = $datas['email'];
        $this->_password = $datas['password'];
    }

    public function createImage() {
        $this->_db->exec('INSERT INTO montage (id_user) VALUES(:id_user);', ['id_user' => $this->getId()]);
        $ids = $this->_db->get('SELECT MAX(id) AS value FROM montage WHERE id_user=:id_user;', ['id_user' => $this->getId()]);
        return $ids[0]['value'];
    }

    public function createTokenConnexion() {
        return $this->createToken(1);
    }

    public function createTokenResetPassword() {
        return $this->createToken(2);
    }

    public function createTokenValidation() {
        return $this->createToken(3);
    }

    private function createToken($type) {
        $token = $this->generateToken();
        $this->_db->exec('INSERT INTO token (value, id_user, action) VALUES (:token, :id_user, :action)', [
            'token'     => $token,
            'id_user'   => $this->_id,
            'action'    => $type
        ]);
        return $token;
    }

    private function generateToken() {
        do {
            $token = str_shuffle(md5(time()));
        } while (count($this->_db->get('SELECT id FROM token WHERE value=:token;', ['token' => $token])));
        return $token;
    }

    /**
     * Gets the value of _login.
     *
     * @return mixed
     */
    public function getLogin()
    {
        return $this->_login;
    }

    /**
     * Sets the value of _login.
     *
     * @param mixed $_login the login
     *
     * @return self
     */
    public function _setLogin($login)
    {
        $this->_login = $login;

        return $this;
    }

    /**
     * Gets the value of _email.
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * Sets the value of _email.
     *
     * @param mixed $_email the email
     *
     * @return self
     */
    public function _setEmail($email)
    {
        $this->_email = $email;

        return $this;
    }

    /**
     * Gets the value of _isConnected.
     *
     * @return mixed
     */
    public function connected()
    {
        return $this->_isConnected;
    }

    /**
     * Sets the value of _isConnected.
     *
     * @param mixed $_isConnected the is connected
     *
     * @return self
     */
    public function _setIsConnected($isConnected)
    {
        $this->_isConnected = $isConnected;

        return $this;
    }

    /**
     * Gets the value of _password.
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * Sets the value of _password.
     *
     * @param mixed $_password the password
     *
     * @return self
     */
    private function _setPassword($password)
    {
        $this->_password = $password;

        return $this;
    }

    /**
     * Gets the value of _id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Sets the value of _id.
     *
     * @param mixed $_id the id
     *
     * @return self
     */
    private function _setId($id)
    {
        $this->_id = $id;

        return $this;
    }
}