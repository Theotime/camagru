<?php

return [
	[
		"pattern"		=> "/",
		"class"			=> "Home",
		"methode"		=> "home",
		"visibility"	=> "all",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/signin",
		"class"			=> "User",
		"methode"		=> "signin",
		"visibility"	=> "visitor",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/signin-post",
		"class"			=> "User",
		"methode"		=> "signinAction",
		"visibility"	=> "visitor",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/signup",
		"class"			=> "User",
		"methode"		=> "signup",
		"visibility"	=> "visitor",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/signup-post",
		"class"			=> "User",
		"methode"		=> "signupAction",
		"visibility"	=> "visitor",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/validation/([a-zA-Z0-9]+)",
		"class"			=> "User",
		"methode"		=> "validationAccount",
		"visibility"	=> "visitor",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/signout",
		"class"			=> "User",
		"methode"		=> "signout",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/reset-password",
		"class"			=> "User",
		"methode"		=> "resetPassword",
		"visibility"	=> "visitor",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/reset-password-post",
		"class"			=> "User",
		"methode"		=> "resetPasswordAction",
		"visibility"	=> "visitor",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/me",
		"class"			=> "User",
		"methode"		=> "me",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/me-post",
		"class"			=> "User",
		"methode"		=> "mePost",
		"visibility"	=> "user",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/gallery/me",
		"class"			=> "Gallery",
		"methode"		=> "me",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/me/page/([0-9]+)",
		"class"			=> "Gallery",
		"methode"		=> "getMyPagingation",
		"visibility"	=> "all",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/gallery",
		"class"			=> "Gallery",
		"methode"		=> "gallery",
		"visibility"	=> "all",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/page/([0-9]+)",
		"class"			=> "Gallery",
		"methode"		=> "getPagingation",
		"visibility"	=> "all",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/gallery/([0-9]+)",
		"class"			=> "Gallery",
		"methode"		=> "picture",
		"visibility"	=> "all",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/([0-9]+)/like",
		"class"			=> "Gallery",
		"methode"		=> "like",
		"visibility"	=> "user",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/gallery/([0-9]+)/unlike",
		"class"			=> "Gallery",
		"methode"		=> "unlike",
		"visibility"	=> "user",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/gallery/([0-9]+)/comment",
		"class"			=> "Gallery",
		"methode"		=> "comment",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/([0-9]+)/delete",
		"class"			=> "Gallery",
		"methode"		=> "delete",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/new/webcam",
		"class"			=> "Gallery",
		"methode"		=> "newWebcamPicture",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/new/webcam-action",
		"class"			=> "Gallery",
		"methode"		=> "newWebcamPictureAction",
		"visibility"	=> "user",
		"type"			=> "ajax"
	],
	[
		"pattern"		=> "/gallery/new/upload",
		"class"			=> "Gallery",
		"methode"		=> "newUploadPicture",
		"visibility"	=> "user",
		"type"			=> "page"
	],
	[
		"pattern"		=> "/gallery/new/upload-action",
		"class"			=> "Gallery",
		"methode"		=> "newUploadPictureAction",
		"visibility"	=> "user",
		"type"			=> "page"
	],
];