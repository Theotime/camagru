<?php

function __autoload($className) {
	$arg = split("\\\\", $className);
	if (count($arg) > 1 && file_exists(__DIR__. '/../'. $arg[0] .'/'. $arg[1]. '.php'))
		require_once strtolower(__DIR__. '/../'. $arg[0]) .'/'. $arg[1]. '.php';
}

$db = new Core\Database();
$pdo = $db->getPdo();

try {
	$pdo->exec('DROP TABLE token;');
	$pdo->exec('DROP TABLE comment;');
	$pdo->exec('DROP TABLE thumb;');
	$pdo->exec('DROP TABLE montage;');
	$pdo->exec('DROP TABLE user;');

	$pdo->exec('CREATE TABLE user (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		login VARCHAR(50) NOT NULL,
		email VARCHAR(150) NOT NULL,
		password VARCHAR(32) NOT NULL,
		is_actif BOOL NOT NULL DEFAULT false,
		CONSTRAINT pk_id_user PRIMARY KEY (id),
		UNIQUE (login),
		UNIQUE (email)
	);');

	$pdo->exec('CREATE TABLE montage (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		id_user INT UNSIGNED NOT NULL,
		CONSTRAINT pk_id_montage PRIMARY KEY (id),
		CONSTRAINT fk_montage_id_user FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE CASCADE
	);');

	$pdo->exec("CREATE TABLE thumb (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		id_user INT UNSIGNED NOT NULL,
		id_montage INT UNSIGNED NOT NULL,
		CONSTRAINT pk_id_thumb PRIMARY KEY (id),
		CONSTRAINT fk_thumb_id_montage FOREIGN KEY (id_montage) REFERENCES montage(id) ON DELETE CASCADE,
		CONSTRAINT fk_thumb_id_user FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE CASCADE,
		UNIQUE (id_montage, id_user)
	);");

	$pdo->exec('CREATE TABLE comment (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		id_user INT UNSIGNED NOT NULL,
		id_montage INT UNSIGNED NOT NULL,
		value TEXT NOT NULL,
		CONSTRAINT pk_id_comment PRIMARY KEY (id),
		CONSTRAINT fk_comment_id_montage FOREIGN KEY (id_montage) REFERENCES montage(id) ON DELETE CASCADE,
		CONSTRAINT fk_comment_id_user FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE CASCADE
	);');

	$pdo->exec('CREATE TABLE token (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		value VARCHAR(255) NOT NULL,
		id_user INT UNSIGNED NOT NULL,
		action INT UNSIGNED NOT NULL,
		consume BOOL NOT NULL DEFAULT false,
		CONSTRAINT pk_id_token PRIMARY KEY (id),
		CONSTRAINT fk_toker_id_user FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE CASCADE,
		UNIQUE(value)
	);');

} catch  (Exception $e) {
	echo $e->getMessage();
}