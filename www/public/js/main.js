'use strict';

var Ajax = function (url, data) {
	this.http = this.createXMLHttp();
	this.url = url;
	if (data != undefined)
		this.data = this.format(data);
};

Ajax.prototype.format = function (data, index) {
	var result = '';
	var start = false;

	for (var i in data) {
		if (i == '__proto__')
			continue ;
		if (start)
			result += '&';
		if (index != undefined)
			result += (typeof data[i] == 'object' ? this.format(data[i], index == undefined ? i : index +'['+ i +']') : index +'['+ i +']='+ encodeURIComponent(data[i]));
		else
			result += (typeof data[i] == 'object' ? this.format(data[i], index == undefined ? i : index +'['+ i +']') : i +'='+ encodeURIComponent(data[i]));
		start = true;
	}
	return result;
} 

Ajax.prototype.send = function (type, callback) {
	this.http.open(type, this.url, true);
	this.http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	if (this.data != undefined  && this.data.length)
		this.http.send(this.data);
	else
		this.http.send();
	var self = this;

	var onChange = function() {
		if (self.http.readyState == 4)
			callback(this.http.responseText);
	};

	this.http.onreadystatechange = onChange.bind(this);
};

Ajax.prototype.post = function(callback) {
	this.send('post', callback);
};

Ajax.prototype.get = function(callback) {
	this.send('get', callback);	
};

Ajax.prototype.createXMLHttp = function () {
	if (typeof XMLHttpRequest !== undefined) {
		return new XMLHttpRequest;
	} else if (window.ActiveXObject) {
		var ieXMLHttpVersions = ['MSXML2.XMLHttp.5.0', 'MSXML2.XMLHttp.4.0', 'MSXML2.XMLHttp.3.0', 'MSXML2.XMLHttp', 'Microsoft.XMLHttp'];
		var xmlHttp;
		for (var i = 0; i < ieXMLHttpVersions.length; i++) {
			try {
				xmlHttp = new ActiveXObject(ieXMLHttpVersions[i]);
				return xmlHttp;
			} catch (e) {}
		}
	}
};

(function () {
	var likes = document.querySelectorAll('.like');

	for (var i = 0; i < likes.length; ++i) {
		likes[i].addEventListener('click', function (e) {
			var classes = this.classList;
			var id = this.getAttribute('data-id');
			var elem = this;
			e.preventDefault();

			if (classes.contains('on')) {
				new Ajax('/gallery/'+ id +'/unlike').get(function (data) {
					var json = JSON.parse(data);
					if (json.success)
						elem.className = 'like off';
				});
			} else {
				new Ajax('/gallery/'+ id +'/like').get(function (data) {
					var json = JSON.parse(data);
					if (json.success)
						elem.className = 'like on';
				});
			}
		});
	}
})();

(function () {
	var form = document.querySelector('#resetPassword');

	if (!form)
		return ;
	form.addEventListener('submit', function (e) {
		e.preventDefault();
		var action = this.getAttribute('action');
		var email = document.querySelector('#email');
		var datas = {
			'resetPassword' : {
				'email' : email.value
			}
		};
		new Ajax(action, datas).post(function (data) {
			var json = JSON.parse(data);
			alert(json.message);
			if (json.success) {
				email.value = '';
			}
		});
	});
})();

(function () {
	var resetPasswordForm = document.querySelector('#meForm');

	if (!resetPasswordForm)
		return ;
	resetPasswordForm.addEventListener('submit', function (e) {
		e.preventDefault();
		var action = this.getAttribute('action');
		var oldPassword = document.querySelector('#oldpassword');
		var newPassword = document.querySelector('#password');
		var datas = {
			'me' : {
				'oldPassword' : oldPassword.value,
				'newPassword' : newPassword.value
			}
		};
		new Ajax(action, datas).post(function (data) {
			var json = JSON.parse(data);
			
			if (json.success) {
				oldPassword.value = '';
				newPassword.value = '';
			} else {
				alert(json.message);
			}
		});
	});
})();

(function () {
	var form = document.querySelector('#signup');

	if (!form)
		return ;
	form.addEventListener('submit', function (e) {
		e.preventDefault();
		var action = this.getAttribute('action');
		var login = document.querySelector('#login');
		var email = document.querySelector('#email');
		var password = document.querySelector('#password');
		var datas = {
			'signup' : {
				'login' : login.value,
				'email' : email.value,
				'password' : password.value
			}
		};
		new Ajax(action, datas).post(function (data) {
			var json = JSON.parse(data);
			alert(json.message);
			if (json.success) {
				login.value = '';
				password.value = '';
				email.value = '';
			}
		});
	});
})();

(function () {
	var form = document.querySelector('#signin');

	if (!form)
		return ;
	form.addEventListener('submit', function (e) {
		e.preventDefault();
		var action = this.getAttribute('action');
		var login = document.querySelector('#login');
		var password = document.querySelector('#password');
		var datas = {
			'signin' : {
				'login' : login.value,
				'password' : password.value
			}
		};
		new Ajax(action, datas).post(function (data) {
			var json = JSON.parse(data);
			if (json.success) {
				window.location.reload();
			} else {
				alert(json.message);
				password.value = '';
			}
		});
	});
})();

(function(){
	var button = document.querySelector('#paginationButton');
	var timeline = document.querySelector('div.timeline');
	if (!button)
		return ;
	var url = button.getAttribute('data-url');
	button.addEventListener('click', function (e) {
		var page = button.getAttribute('data-page');
		new Ajax(url + page).get(function (html) {
			var elements = JSON.parse(html);
			button.parentNode.removeChild(button);
			var element = document.querySelector('div.montage');
			for (var i = 0; i < elements.length; ++i) {
				var clone = element.cloneNode(true);
				clone.querySelector('a').setAttribute('href', '/gallery/'+ elements[i].img_id);
				clone.querySelector('div.img').style.backgroundImage = "url('/img/montage/montage-"+ elements[i].img_id +".jpg')";
				clone.querySelector('b.author').innerHTML = elements[i].login;
				clone.querySelector('img').setAttribute('src', 'http://www.gravatar.com/avatar/'+ elements[i].avatar);
				if (clone.querySelector('a.delete'))
					clone.querySelector('a.delete').setAttribute('href', '/gallery/'+ elements[i].img_id +'/delete');
				if (clone.querySelector('.like')) {
					clone.querySelector('span.like').setAttribute('data-id', elements[i].img_id);
					if (elements[i].liked == 1)
					clone.querySelector('span.like').className = 'like on';
					else
						clone.querySelector('span.like').className = 'like off';
					clone.querySelector('.like').addEventListener('click', function (e) {
						var classes = this.classList;
						var id = this.getAttribute('data-id');
						var elem = this;
						e.preventDefault();

						if (classes.contains('on')) {
							new Ajax('/gallery/'+ id +'/unlike').get(function (data) {
								var json = JSON.parse(data);
								if (json.success)
									elem.className = 'like off';
							});
						} else {
							new Ajax('/gallery/'+ id +'/like').get(function (data) {
								var json = JSON.parse(data);
								if (json.success)
									elem.className = 'like on';
							});
						}
					});
				}

				timeline.appendChild(clone);
			}
			if (elements.length == 5) {
				button.setAttribute('data-page', parseInt(page) + 1);
				timeline.appendChild(button);
			}
		});
	});

})();


(function() {
	if (window.location.pathname != '/gallery/new/webcam')
		return ;

	var streaming		= false;
	var video			= document.querySelector('#video');
	var previewVideo	= document.querySelector('#calques');
	var cover			= document.querySelector('#cover');
	var canvas			= document.querySelector('#canvas');
	var photo			= document.querySelector('#photo');
	var startbutton		= document.querySelector('#startbutton');
	var montages		= {};
	var width			= 640;
	var height			= 0;

	navigator.getMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
	navigator.getMedia({ video: true, audio: false },
		function(stream) {
			if (navigator.mozGetUserMedia) {
				video.mozSrcObject = stream;
			} else {
				var vendorURL = window.URL || window.webkitURL;
				video.src = vendorURL.createObjectURL(stream);
			}
			video.play();
		},
		function(err) {
			console.log("An error occured! " + err);
		}
	);

	video.addEventListener('canplay', function(ev) {
		if (!streaming) {
			height = video.videoHeight / (video.videoWidth/width);
			video.setAttribute('width', '600px');
			video.setAttribute('height', '450px');
			streaming = true;
		}
	}, false);

	function takepicture(montages) {
		canvas.width = width;
		canvas.height = height;
		canvas.getContext('2d').drawImage(video, 0, 0, width, height);
		var data = canvas.toDataURL('image/png');
		var mont = [];
		for (var i in montages) {
			mont.push(montages[i]['image']);
		}
		new Ajax('/gallery/new/webcam-action', {
			'img' : data,
			'montages' : mont
		}).post(function (data) {
			var json = JSON.parse(data);

			if (json.success) {
				photo.style.backgroundImage =  'url("")';
				photo.style.backgroundImage =  'url("'+ json.src +'")';
				photo.style.width = video.offsetWidth +'px';
				photo.style.height = video.offsetHeight +'px';
			} else {
				alert(json.message);
			}
		});
	}

	startbutton.addEventListener('click', function(ev){
		if (this.classList.contains('disabled'))
			return ;
		takepicture(montages);
		ev.preventDefault();
		for (var i in montages) {
			montages[i]['element'].className = 'element montage';
			previewVideo.removeChild(montages[i]['img']);
			delete montages[i];
		}
		startbutton.className = 'disabled';
	}, false);


	var elements = document.querySelectorAll('.element');
	for (var i = 0; i < elements.length; ++i) {
		if (elements[i].classList.contains('placeholder'))
			continue ;
		elements[i].addEventListener('click', function () {
			var image = this.getAttribute('data-image');
			if (this.classList.contains('active')) {
				this.className = 'element montage';
				previewVideo.removeChild(montages[image]['img']);
				delete montages[image]['img'];
				delete montages[image];
				if (!Object.keys(montages).length)
					startbutton.className = 'disabled';
			} else {
				this.className = 'element montage active';
				var img = document.createElement('img');
				img.setAttribute('src', '/img/component/'+ image);
				img.className = 'imgMontage';
				montages[image] = {
					'element' : this,
					'img' : img,
					'image' : image
				};
				img.addEventListener('dragstart', function () {
					img.className = 'imgMontage move';
				});

				img.addEventListener('dragend', function (e) {
					img.className = 'imgMontage';
				});

				previewVideo.appendChild(img);
				startbutton.className = '';
			}
		});
	}

})();