<?php
	session_start();
	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', '-1'); 
	function __autoload($className) {
		$arg = split("\\\\", $className);
		if (count($arg) > 1 && file_exists($arg[0] .'/'. $arg[1]. '.php'))
			require_once strtolower($arg[0]) .'/'. $arg[1]. '.php';
	}

	$db = new Core\Database();
	$user = new Core\User($db);
	$router = new Core\Router(__DIR__ ."/config/routes.php", $user);
	$route = $router->match();
	$page = "Page\\". $route['class'];
	$page = new $page($user, $router, $db);

	$json = $page->$route['methode']();

	if ($route['type'] == 'page')
		include __DIR__ ."/template/base.phtml";
	else
		echo json_encode($json);

?>