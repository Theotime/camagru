all: build run

build:
	docker build -t triviere/camagru .

run:
	docker run --rm -it -p 8000:80 -p 3306:3306 -v "$$(pwd)/www:/var/www/html/" triviere/camagru bash

launch:
	docker run --rm -p 8000:80 -p 3306:3306 -v "$$(pwd)/www:/var/www/html" triviere/camagru bash
